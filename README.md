# An experiment...

This project represents an idea I had for using bayesian networks to automatically categorise posts from blogs.  It never really worked all that well.  I no longer have the corpus of words that originally used to train the network, so there's no way to demo this.

I'm keeping it here so I can raid it for code snippets.  I might resurrect at some point in the future.
